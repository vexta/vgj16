﻿using UnityEngine;
using System.Collections;

public class WeaponBase : MonoBehaviour 
{
	public Transform WhoHasThisWeapon;

	public GameObject ProjectilePrefab;

	public float ProjectilesPerSecond = 1;

	private float _elapsed = 100f, _cooldownDurationSeconds;

	void Start () 
	{
		
	}
	
	void Update () 
	{
		_cooldownDurationSeconds = 1f / ProjectilesPerSecond;
		_elapsed += Time.deltaTime;
	}

	public void Fire()
	{
		if (_elapsed > _cooldownDurationSeconds)
		{
			var go = (GameObject)Instantiate (ProjectilePrefab, transform.position, transform.rotation);
			var projectile = go.GetComponent<Projectile> ();
			projectile.WhoFired = WhoHasThisWeapon;

			_elapsed = 0;
		}
	}
}
