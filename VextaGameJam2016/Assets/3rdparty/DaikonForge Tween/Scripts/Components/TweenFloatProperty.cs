﻿/* Copyright 2013-2015 Daikon Forge */
using UnityEngine;

using System;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

using DaikonForge.Tween;

namespace DaikonForge.Tween.Components
{

	/// <summary>
	/// Used to animate a named property of type float
	/// </summary>
	[AddComponentMenu( "Daikon Forge/Tween/Named Float" )]
	public class TweenFloatProperty : TweenPropertyComponent<float>
	{
	}

}

