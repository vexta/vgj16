﻿using UnityEngine;
using System.Collections;

using DaikonForge.Tween;
using DaikonForge.Tween.Interpolation;

using UnityEngine.UI;

public class UGUI_Doge : MonoBehaviour
{

	public Image doge;
	public Text[] labels;
	public Text logo;

	void Start()
	{

		// Using a TweenTimeline allows us to specify the exact time that the
		// tweens should start. It is somewhat more flexible than using TweenGroup,
		// especially when you need both sequential and concurrent behavior.
		var timeline = new TweenTimeline();

		var dogeScale = doge.TweenScale()
			.SetStartValue( Vector3.zero )
			.SetDuration( 0.5f )
			.SetDelay( 0.5f )
			.SetInterpolator( EulerInterpolator.Default )
			.SetEasing( TweenEasingFunctions.Spring );

		var dogeAlpha = doge.TweenAlpha()
			.SetStartValue( 0f )
			.SetEndValue( 1f )
			.SetDelay( 0.5f )
			.SetDuration( 0.5f );

		timeline.Add( 0, dogeScale );
		timeline.Add( 0, dogeAlpha );

		for( int i = 0; i < labels.Length; i++ )
		{

			// Need to start with the TextMesh invisible (it is visible during design time for convenience)
			labels[ i ].color = new Color( 1, 1, 1, 0 );

			var alphaTween = labels[ i ].TweenAlpha()
				.SetAutoCleanup( true )
				.SetDuration( 0.33f )
				.SetStartValue( 0f )
				.SetEndValue( 1f );

			var rotTween = labels[ i ].TweenRotation()
				.SetAutoCleanup( true )
				.SetStartValue( Vector3.up * -90 )
				.SetEndValue( Vector3.zero )
				.SetDuration( 0.5f )
				.SetEasing( TweenEasingFunctions.Spring );

			// Note that we can add as many tweens as we like at the same 
			// time in the timeline. They will all run concurrently when 
			// that time is reached.
			timeline.Add( 1.5f + 0.75f * i, alphaTween, rotTween );

		}

		var parent = doge.transform.parent.GetComponent<Graphic>();
		var dogeSlide = parent.TweenPosition()
			.SetEndValue( transform.position + new Vector3( 0f, 100f, 0f ) )
			.SetDuration( 0.5f )
			.SetEasing( TweenEasingFunctions.EaseInOutQuad );

		var logoSlide = logo.TweenPosition()
			.SetStartValue( logo.rectTransform.anchoredPosition3D - ( Vector3.up * 100f ) )
			.SetEndValue( logo.rectTransform.anchoredPosition3D + ( Vector3.up * 100f ) )
			.SetDuration( 1f )
			.SetEasing( TweenEasingFunctions.Bounce );

		var logoAlphaTween = logo.TweenAlpha()
				.SetStartValue( 0f )
				.SetEndValue( 1f )
				.SetDuration( 0.75f );

		// Need to start with the logo invisible (it is visible during design time for convenience)
		logo.color = new Color( 1, 1, 1, 0 );

		timeline.Add( 3.75f, dogeSlide );
		timeline.Add( 4f, logoSlide, logoAlphaTween );

		timeline.Play();

	}

}