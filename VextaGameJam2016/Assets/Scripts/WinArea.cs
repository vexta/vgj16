﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;

public class WinArea : MonoBehaviour
{
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.GetComponent<CharacteContoler>() != null)
        {
            Mutation.Mutate();
            UnityEngine.SceneManagement.SceneManager.LoadScene(1);
        }
    }
}
