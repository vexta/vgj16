﻿using UnityEngine;
using System.Collections;

public class LookAtCamera : MonoBehaviour
{
    Vector3 position { get { return transform.position; } set { transform.position = value; } }

    void Start ()
    {

	}
	
	void Update ()
    {
        var cameraDirection = (Camera.main.transform.position - position).normalized;
        transform.rotation = Quaternion.LookRotation(cameraDirection) * Quaternion.Euler(0, 180, 0);
    }
}