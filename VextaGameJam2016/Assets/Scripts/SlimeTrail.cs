﻿using UnityEngine;
using System.Collections;

public class SlimeTrail : MonoBehaviour
{
    public float Damage = 7.5f;

	void Start ()
    {
        Invoke("DeleteMe", 5);
	}
	
    void DeleteMe()
    {
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider col)
    {
        var player = col.gameObject.GetComponent<CharacteContoler>();
        if (col.gameObject.GetComponent<CharacteContoler>() != null)
        {
            player.Health -= Damage;
            DeleteMe();
        }
    }
}
