﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets.Scripts;

public class CharacteContoler : Character
{
    public float Speed = 3;
    public float CameraDistance = 20;
    public Camera MainCamera;
	public Transform CharacterSprite; 
    public Texture2D CharacterLeft;
    public Texture2D CharacterRight;
    public Texture2D CharacterFront;
    public Texture2D CharacterBack;
	public WeaponBase Weapon;
    public Image HealthBar;

    Vector3 Position { get { return transform.position; } set { transform.position = value; } }
    Material mainMaterial;

    void OnTriggerEnter(Collider col)
    {
    }

	void OnCollsionEnter(Collision col) 
	{
		Debug.Log ("Collided with stuff");
	}

    void Start ()
    {
        mainMaterial = CharacterSprite.GetComponent<MeshRenderer>().material;
        Health = Mutation.HealthMutation;
	}
	
	void Update ()
    {
        if (Health <= 0)
            SceneManager.LoadScene(2);

        HealthBar.color = new Color((100 - Health) / 100, (Health / 100), 0);
        HealthBar.transform.localScale = new Vector3(Health / 100, 1, 1);

		var ls_dx = Input.GetAxisRaw ("Horizontal");
		var ls_dy = Input.GetAxisRaw ("Vertical");

		var vel = new Vector3 (ls_dx, 0, ls_dy);
		if (vel.sqrMagnitude > 0.2)
		{
			//vel.Normalize ();
			transform.Translate (vel * Speed * Time.deltaTime);


		}

		MainCamera.transform.position = transform.position + -MainCamera.transform.forward * CameraDistance;

		if (Weapon)
		{
			if (!Weapon.WhoHasThisWeapon)
				Weapon.WhoHasThisWeapon = transform;

			#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
			var rs_dx = Input.GetAxisRaw ("RSHorizontalMac");
			var rs_dy = -Input.GetAxisRaw ("RSVerticalMac");
			#else
			var rs_dx = Input.GetAxisRaw ("RSHorizontalWin");
			var rs_dy = -Input.GetAxisRaw ("RSVerticalWin");
			#endif

			var rsDir = new Vector3 (rs_dx, 0, rs_dy);
			if (rsDir.sqrMagnitude > 0.2)
			{
				Weapon.transform.forward = rsDir.normalized;

				if (rs_dy == 0 && rs_dx == 0)
					mainMaterial.mainTexture = CharacterFront;
				else if (rs_dy > 0 && rs_dy > rs_dx)
					mainMaterial.mainTexture = CharacterBack;
				else if (rs_dy < 0 && rs_dy < rs_dx)
					mainMaterial.mainTexture = CharacterFront;
				else if (rs_dx < 0)
					mainMaterial.mainTexture = CharacterLeft;
				else
					mainMaterial.mainTexture = CharacterRight;
				
			} else
			{
				Weapon.transform.forward = Vector3.forward;
			}


			#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
			var rt = Input.GetAxisRaw ("RightTriggerMac");
			#else
			var rt = Input.GetAxisRaw ("RightTriggerWin");
			#endif

			if (rt > 0)
				Weapon.Fire ();
		}
    }
}