﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.AI
{
    public class Rocky : Enemy
    {
        const float ChargeRange = 10;
        const float RetreatRange = 2;

        void OnTriggerEnter(Collider col)
        {
            var player = col.gameObject.GetComponent<CharacteContoler>();
            if (player != null)
            {
                player.Health -= TouchDamage;
                action = EnemyAction.Retreat;
            }
        }

        protected override EnemyAction WhatToDo()
        {
			if (!IsPlayerVisible ())
				return EnemyAction.Wait;

            if ((Player.position - transform.position).magnitude < RetreatRange)
                return EnemyAction.Retreat;

            if ((Player.position - transform.position).magnitude < ChargeRange)
                return EnemyAction.Charge;

            return EnemyAction.Wait;
        }
    }
}
