﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public enum EnemyAction
{
    Wait, MoveTowards, Dodge, Retreat, Charge, MoveFree
}

public class Enemy : Character
{
    static List<Enemy> enemies = new List<Enemy>();

    public Transform Player;
    public float ShootSpeed = 0.5f;
    public float MovementSpeed { get { return navAgent.speed; } set { navAgent.speed = value; } }
    public float ChargeSpeed = 5;
    public float TouchDamage = 20;

    public Texture2D CharacterLeft;
    public Texture2D CharacterRight;
    public Texture2D CharacterFront;
    public Texture2D CharacterBack;

    public WeaponBase Weapon;

    NavMeshAgent navAgent;
    float movementTimer = 0;
    Material mainMaterial;
    Vector3 lastPosition;
    Vector3 freeMoveDirection = new Vector3(1, 0, 0).normalized;
    protected EnemyAction action = EnemyAction.Wait;

    protected virtual void Start()
    {
		Player = GameObject.FindGameObjectWithTag ("Player").transform;
        Health = 60;
        enemies.Add(this);
        GetComponent<NavMeshAgent>().updateRotation = false;
        mainMaterial = GetComponent<MeshRenderer>().material;
        navAgent = GetComponent<NavMeshAgent>();

		if (Weapon)
        	Weapon.WhoHasThisWeapon = transform;
    }

    void Update()
    {
        movementTimer += Time.deltaTime;

        if (Health <= 0)
        {
            Destroy(gameObject);
            return;
        }

        UpdateAI();
        UpdateSprite(position - lastPosition);
        lastPosition = transform.position;
    }

    protected virtual void UpdateAI()
    {
        switch (action)
        {
            case EnemyAction.Wait:
                navAgent.Stop();
                action = WhatToDo();
                break;
            case EnemyAction.MoveTowards:
                MoveTowards();
                break;
            case EnemyAction.Dodge:
                navAgent.Stop();
                Dodge();
                break;
            case EnemyAction.Charge:
                Charge();
                break;
            case EnemyAction.Retreat:
                Retreat();
                break;
            case EnemyAction.MoveFree:
                MoveFree();
                break;
        }

		if (Weapon && (action == EnemyAction.MoveTowards || action == EnemyAction.Dodge))
        {
			Weapon.transform.forward = (Player.position - position).normalized;
			Weapon.Fire();
        }
    }

    void Dodge()
    {
        if (movementTimer > 3)
        {
            action = WhatToDo();
            movementTimer = 0;
        }
        else
        {
            float direction = (int)movementTimer % 2 == 0 ? 1 : -1;
            navAgent.Move(Vector3.Cross((Player.position - position).normalized, new Vector3(0, 1, 0)) * Time.deltaTime * MovementSpeed * direction);
        }
    }

    void MoveTowards()
    {
        if (movementTimer > 3)
        {
            action = WhatToDo();
            movementTimer = 0;
        }
        else if ((int)movementTimer != (int)(movementTimer - Time.deltaTime))
        {
            navAgent.Resume();
            Debug.Log(navAgent.SetDestination(Player.position));
        }
    }

    void Charge()
    {
        if (_attackScream != null)   AudioSource.PlayClipAtPoint(_attackScream, position);
        if (movementTimer > 3)
        {
            action = WhatToDo();
            movementTimer = 0;
        }
        else
            navAgent.Move((Player.position - position).normalized * ChargeSpeed * Time.deltaTime);
    }

    void Retreat()
    {
        if (movementTimer > 2)
        {
            action = WhatToDo();
            movementTimer = 0;
        }
        else
            navAgent.Move(-(Player.position - position).normalized * MovementSpeed * Time.deltaTime);
    }

    void MoveFree()
    {
        if (movementTimer >= 1 && UnityEngine.Random.Range(0, (int)(1 / Time.deltaTime) * 1) == 0)
        {
            freeMoveDirection = GetRandomDirection(freeMoveDirection);
            movementTimer = 0;
        }

        var movement = freeMoveDirection * MovementSpeed * Time.deltaTime;
        NavMeshHit hit;
        if (navAgent.Raycast(movement + position, out hit))
        {
            freeMoveDirection = GetRandomDirection(freeMoveDirection);
            Debug.Log("HIT");
        }
        navAgent.Move(movement);

        action = WhatToDo();
    }

    Vector3 GetRandomDirection(Vector3 currentDirection)
    {
        NavMeshHit hit;
        var directions = new Vector3[] { new Vector3(1, 0, 0), new Vector3(-1, 0, 0), new Vector3(0, 0, 1), new Vector3(0, 0, -1) };
        directions = directions.Where(x => !navAgent.Raycast(position + x, out hit) && x != -currentDirection).ToArray();

        if (directions.Length == 0)
            return -currentDirection;
        else
            return directions[UnityEngine.Random.Range(0, directions.Length)];
    }

    void UpdateSprite(Vector3 desiredVelocity)
    {
        if (desiredVelocity.magnitude < 0.01f)
            mainMaterial.mainTexture = CharacterFront;

        else if (Math.Abs(desiredVelocity.x) > Math.Abs(desiredVelocity.z))
        {
            if (desiredVelocity.x > 0)
                mainMaterial.mainTexture = CharacterRight;
            else
                mainMaterial.mainTexture = CharacterLeft;
        }
        else
        {
            if (desiredVelocity.z > 0)
                mainMaterial.mainTexture = CharacterBack;
            else
                mainMaterial.mainTexture = CharacterFront;
        }
    }

	protected bool IsPlayerVisible()
	{
		var dir = (Player.position - transform.position).normalized;
		RaycastHit hit;
		if (Physics.Raycast (transform.position, dir, out hit, 10))
			return hit.transform == Player;
		return false;
	}

    protected bool IsPlayerInRange()
    {
        return (Player.position - transform.position).magnitude < 10;
    }

    protected virtual EnemyAction WhatToDo()
    {
		if (!IsPlayerVisible())
            return EnemyAction.Wait;

        var distance = (Player.position - transform.position).magnitude / 5; // Between 0 & 2
        var rnd = (int)(UnityEngine.Random.Range(0, 7) * distance);
        if (rnd * distance < 3) return EnemyAction.Dodge;
        else return EnemyAction.MoveTowards;
    }
}