﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.AI
{
    public class Slimy : Enemy
    {
        public GameObject SlimeTrailPrefab;
        public float SlimeTrailSpawnSpeed = 1;

        float slimeSpawnTime = 0;

        public Slimy()
        {

        }

        void OnTriggerEnter(Collider col)
        {
            var player = col.gameObject.GetComponent<CharacteContoler>();
            if (player != null)
            {
                player.Health -= TouchDamage;
            }
        }

        protected override void Start()
        {
            base.Start();
            MovementSpeed = 2.5f;
        }

        protected override void UpdateAI()
        {
            base.UpdateAI();

            slimeSpawnTime += Time.deltaTime;
            if (slimeSpawnTime >= 1 / SlimeTrailSpawnSpeed)
            {
                var trail = Instantiate(SlimeTrailPrefab);
                trail.transform.position = new Vector3(position.x, 0.001f, position.z);
                slimeSpawnTime = 0;
            }
        }

        protected override EnemyAction WhatToDo()
        {
            if ((Player.position - transform.position).magnitude < 2)
                return EnemyAction.MoveTowards;
            else
                return EnemyAction.MoveFree;
        }
    }
}
