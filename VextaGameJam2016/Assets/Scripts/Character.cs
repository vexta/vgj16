﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour
{
    public float Health = 100;

    protected Vector3 position { get { return transform.position; } set { transform.position = value; } }

    public AudioClip _attackScream;
    public AudioClip _hurtScream ;
    public AudioClip _deathScream ;

    public void WasHit(float damage)
    {
        Health -= damage;
        if (Health <= 0)
        {
            if (_deathScream != null)   AudioSource.PlayClipAtPoint(_deathScream, position);
        }
        else
        {
            if (_hurtScream != null)    AudioSource.PlayClipAtPoint(_hurtScream, position);
        }
    }
}
