﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public static class Mutation
    {
        public static float HealthMutation = 100;
        public static float DamageMutation = 0;

        public static void Mutate()
        {
            if (Random.Range(0, 2) == 0)
                HealthMutation = Random.Range(20, 11);
            else
                DamageMutation = Random.Range(-5, 10);

            Debug.Log("Health mutation: " + HealthMutation + ". Damage mutation: " + DamageMutation);
        }
    }
}
