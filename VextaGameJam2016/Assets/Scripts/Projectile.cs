﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;

public class Projectile : MonoBehaviour
{
	public Transform WhoFired;
    public float Speed = 10;
    public float Damage = 10;
	public GameObject HitEffectPrefab;

	void Start ()
    {
        if (WhoFired.GetComponent<CharacteContoler>() != null)
            Damage += Mutation.DamageMutation;

		Invoke ("KillSelf", 5);
	}

	void KillSelf() {
		Destroy (gameObject);
	}
	
	void Update ()
    {
		transform.position += transform.forward * Time.deltaTime * Speed;
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.transform == WhoFired)
            return;

        if (HitEffectPrefab)
			Instantiate(HitEffectPrefab, transform.position, Quaternion.identity);
        
		if (col.gameObject.GetComponent<Character>() != null)
		{
			var enemy = col.gameObject.GetComponent<Character> ();
		    enemy.WasHit(Damage);
			Debug.Log("Hit enemy");
		}

		Destroy (gameObject);
    }
}